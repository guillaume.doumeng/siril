#ifndef _LIVESTACK_H
#define _LIVESTACK_H

void on_livestacking_start();
void stop_live_stacking_engine();
void pause_live_stacking_engine();

int get_paused_status();

#endif
